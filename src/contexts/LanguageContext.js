import React from 'react';

const Context = React.createContext('english');

export class LanguageStore extends React.Component {
  state = { lang: 'english' };

  onLangChange = (lang) => {
    this.setState({ lang });
  }

  render() {
    return (
      <Context.Provider
          value={{ ...this.state, onLangChange: this.onLangChange }}>
        {this.props.children}
      </Context.Provider>
    );
  }
}

export default Context
