import React from 'react';

import LanguageContext from '../contexts/LanguageContext';

class LanguageSelect extends React.Component {
  static contextType = LanguageContext;

  render() {
    return (
      <div className="language-selector">
        Select language:
        &nbsp;&nbsp;&nbsp;&nbsp;
        <i 
            className="flag us" 
            onClick={() => this.context.onLangChange('english')} />
        <i 
            className="flag nl" 
            onClick={() => this.context.onLangChange('dutch')} />
      </div>
    );
  }
}

export default LanguageSelect;
