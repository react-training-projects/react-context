import React from 'react';

import LanguageSelect from './LanguageSelect';
import UserCreate from './UserCreate';
import { LanguageStore } from '../contexts/LanguageContext';

class App extends React.Component {
  render() {
    return(
      <div className="ui container">
        <LanguageStore>
          <LanguageSelect />
          <br />
          <br />
          <br />
          <UserCreate />
        </LanguageStore>
      </div>
    );
  }
};

export default App;
